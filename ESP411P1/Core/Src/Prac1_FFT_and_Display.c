#include "Prac1_FFT_and_Display.h"

/*
  * @brief First Principles DFT Function;
  * 		DONT CALL THIS, CALL THE HANDLERS
  * @param LOS
  * @retval LOS
*/
void fp_dft(double complex* signal_dft, const double* input_signal, const int N)
{
	for (int m = 0; m < N; m++)
	{
		for (int n = 0; n < N; n++)
		{
			signal_dft[m] = signal_dft[m] + input_signal[n]*cexp((-I*2*M_PI*m*n)/N);
		}
	}

	return;
}

/*
  * @brief First Principles DFT Handler.
  * @param Pointer to results array, pointer to input signal array,
  * 		N-points for DFT.
  * @retval DFT Result Magnitudes in array pointed to by 1st parameter.
*/
void FirstPrinciples_DFT(double* signal_dft_magnitudes, const double* input_signal, const int N)
{
	double complex signal_dft[N];
	for (int i = 0; i < N; i++) {signal_dft[i] = 0.0 + 0.0*I;}

	fp_dft(signal_dft, input_signal, N);

	double re = 0.0;
	double im = 0.0;
	for (int i = 0; i < N; i++)
	{
		re = creal(signal_dft[i]);
		im = cimag(signal_dft[i]);
		signal_dft_magnitudes[i] = sqrt(re*re + im*im);
	}

	return;
}

/*
  * @brief FFT Twiddle-Factors Generator; Used with FirstPrinciples_FFT_LUT.
  * @param Pointer to results COMPLEX array, N-points for FFT.
  * @retval Complex twiddle-factors pointed to by 1st parameter.
*/
void GenerateTwiddleFactors(double complex* w_factors, const int N)
{
	for (int k = 0; k < N/2; k++)
	{
		w_factors[k] = cexp((-I*2*M_PI*k)/N);
	}

	return;
}

/*
  * @brief First Principles FFT Recursive Function;
  * 		DONT CALL THIS, CALL THE HANDLERS
  * @param LOS
  * @retval LOS
*/
void fp_fft(double complex* signal_fft, const double* input_signal, const int N, const int N_max, const double complex* w_factors)
{
	if (N == 1)
	{
		signal_fft[0] = input_signal[0] + 0.0*I;
		return;
	}
	else
	{
		double input_signal_even[N/2];
		double input_signal_odd[N/2];
		double complex signal_even_fft[N/2];
		double complex signal_odd_fft[N/2];
		for (int i = 0; i < N/2; i++)
		{
			signal_even_fft[i] = 0.0 + 0.0*I;
			signal_odd_fft[i] = 0.0 + 0.0*I;
			input_signal_even[i] = input_signal[2*i];
			input_signal_odd[i] = input_signal[(2*i)+1];
		}
		fp_fft(signal_even_fft, input_signal_even, N/2, N_max, w_factors);
		fp_fft(signal_odd_fft, input_signal_odd, N/2, N_max, w_factors);
		for (int k = 0; k < N/2; k++)
		{
			double complex w_factor = w_factors[k*N_max/N];
			signal_fft[k] = signal_even_fft[k] + (w_factor*signal_odd_fft[k]);
			signal_fft[k+(N/2)] = signal_even_fft[k] - (w_factor*signal_odd_fft[k]);
		}
	}

	return;
}

/*
  * @brief First Principles FFT Handler; Twiddle-Factors calculated on each run.
  * @param Pointer to results array, pointer to input signal array,
  * 		N-points for FFT.
  * @retval FFT Result Magnitudes in array pointed to by 1st parameter.
*/
void FirstPrinciples_FFT(double* signal_fft_magnitudes, const double* input_signal, const int N)
{
	double complex signal_fft[N];
	for (int i = 0; i < N; i++) {signal_fft[i] = 0.0 + 0.0*I;}
	double complex w_factors[N/2];
	GenerateTwiddleFactors(w_factors, N);

	fp_fft(signal_fft, input_signal, N, N, w_factors);

	double re;
	double im;
	for (int i = 0; i < N; i++)
	{
		re = creal(signal_fft[i]);
		im = cimag(signal_fft[i]);
		signal_fft_magnitudes[i] = sqrt(re*re + im*im);
	}

	return;
}

/*
  * @brief First Principles FFT Lookup Table (LUT) Handler
  * @param Pointer to results array, pointer to input signal array,
  * 		N-points for FFT, Pointer to Twiddle-Factors LUT generated using generate_w_factors.
  * @retval FFT Result Magnitudes in array pointed to by 1st parameter.
*/
void FirstPrinciples_FFT_LUT(double* signal_fft_magnitudes, const double* input_signal, const int N, const double complex* w_factors)
{
	double complex signal_fft[N];
	for (int i = 0; i < N; i++) {signal_fft[i] = 0.0 + 0.0*I;}

	fp_fft(signal_fft, input_signal, N, N, w_factors);

	double re;
	double im;
	for (int i = 0; i < N; i++)
	{
		re = creal(signal_fft[i]);
		im = cimag(signal_fft[i]);
		signal_fft_magnitudes[i] = sqrt(re*re + im*im);
	}

	return;
}

/*
  * @brief Screen Layout Draw Function; Draws both axis with intervals and labels;
  * 		Use once at startup before main while (1);
  * 		Screen initialization still required beforehand.
  * @param None
  * @retval None
*/
void DrawLayout()
{
	BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	//Magnitude-axis
	BSP_LCD_DrawHLine(29, 31, 180);
	BSP_LCD_DisplayStringAt(0, 0, (uint8_t*)"Magnitude (V)", CENTER_MODE);
	BSP_LCD_DisplayStringAt(32, 23, (uint8_t*)"0", RIGHT_MODE);
	BSP_LCD_DisplayStringAt(81, 19, (uint8_t*)"1", LEFT_MODE);
	BSP_LCD_DisplayStringAt(135, 19, (uint8_t*)"2", LEFT_MODE);
	BSP_LCD_DisplayStringAt(197, 19, (uint8_t*)"3.3", LEFT_MODE);
	BSP_LCD_DrawVLine(84, 29, 2);
	BSP_LCD_DrawVLine(138, 29, 2);
	BSP_LCD_DrawVLine(207, 29, 2);
	//Frequency-axis
	BSP_LCD_DrawVLine(31, 29, 260);
	BSP_LCD_DisplayStringAtLine(5, (uint8_t*)"F");
	BSP_LCD_DisplayStringAtLine(6, (uint8_t*)"r");
	BSP_LCD_DisplayStringAtLine(7, (uint8_t*)"e");
	BSP_LCD_DisplayStringAtLine(8, (uint8_t*)"q");
	BSP_LCD_DisplayStringAtLine(9, (uint8_t*)"u");
	BSP_LCD_DisplayStringAtLine(10, (uint8_t*)"e");
	BSP_LCD_DisplayStringAtLine(11, (uint8_t*)"n");
	BSP_LCD_DisplayStringAtLine(12, (uint8_t*)"c");
	BSP_LCD_DisplayStringAtLine(13, (uint8_t*)"y");
	//BSP_LCD_DisplayStringAtLine(14, (uint8_t*)" ");
	BSP_LCD_DisplayStringAtLine(15, (uint8_t*)"(");
	BSP_LCD_DisplayStringAtLine(16, (uint8_t*)"k");
	BSP_LCD_DisplayStringAtLine(17, (uint8_t*)"H");
	BSP_LCD_DisplayStringAtLine(18, (uint8_t*)"z");
	BSP_LCD_DisplayStringAtLine(19, (uint8_t*)")");
	BSP_LCD_DisplayStringAt(30, 89, (uint8_t*)"6", RIGHT_MODE);
	BSP_LCD_DisplayStringAt(30, 153, (uint8_t*)"12", RIGHT_MODE);
	BSP_LCD_DisplayStringAt(30, 217, (uint8_t*)"18", RIGHT_MODE);
	BSP_LCD_DisplayStringAt(30, 282, (uint8_t*)"24", RIGHT_MODE);
	BSP_LCD_DrawHLine(29, 95, 2);
	BSP_LCD_DrawHLine(29, 159, 2);
	BSP_LCD_DrawHLine(29, 223, 2);
	BSP_LCD_DrawHLine(29, 287, 2);
	BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);

	return;
}

/*
  * @brief Screen FFT Data Draw Function; Draws FFT data on axis drawn by DrawLayout;
  * 		Use in main while (1) after each N-point FFT calculation;
  * 		Recommended to pass FFT magnitudes calculated using raw 12bit ADC values / N / 4095.
  * @param The Normalized FFT Magnitudes with 0-1 corresponding to 0-3.3V; FFT Size;
  * 		Whether to ignore the DC component in the maximum component display.
  * @retval None
*/
void DrawFFT(const double* fft_mags, const int N, const bool ignore_max_dc)
{
	double value;
	double max = 0;
	int max_index = N/2;

	BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_FillRect(32, 32, 176, 256);

	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
	for (int i = 0; i < N/2; i++)
	{
		if (i > 0 && i < (N/2)-1) value = (fft_mags[i] + fft_mags[N-i])*176.0;
		else value = fft_mags[i]*176.0;

		if (value > max)
		{
			if (i==0) {if (!ignore_max_dc) {max = value;max_index = i;}}
			else {max = value;max_index = i;}
		}

		if (value > 176.0) {value = 176.0;}
		else if (value < 0.0) {value = 0.0;}

		BSP_LCD_DrawHLine(32, 32 + (512/N)*i, value);
	}

	BSP_LCD_SetTextColor(LCD_COLOR_RED);
	if (max >= 3 && (512/N)*max_index >= 3)
		BSP_LCD_DrawCircle(32 + max, 32 + (512/N)*max_index, 3);
	else if (max < 3 && (512/N)*max_index >= 3)
		BSP_LCD_DrawCircle(32 + max + 3, 32 + (512/N)*max_index, 3);
	else if (max >= 3 && (512/N)*max_index < 3)
		BSP_LCD_DrawCircle(32 + max, 32 + (512/N)*max_index + 3, 3);
	else
		BSP_LCD_DrawCircle(32 + max + 3, 32 + (512/N)*max_index + 3, 3);
	char temp_string[10];
	sprintf(temp_string, "%.1f", 1.0*max_index/(N/2)*24.0);
	BSP_LCD_DisplayStringAt(32 + max + 5, 32 + (512/N)*max_index + 5, (uint8_t*)temp_string, LEFT_MODE);
	BSP_LCD_DrawHLine(32, 32 + (512/N)*max_index, max);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);

	return;
}
