from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import urllib.request
import io
import binascii
import numpy as np
from array import array


filePath = "C:\\Users\\Jaco\\Desktop\\MemorySnapshot1"
byte_count = 307200
w, h = 240, 320
raw_data = array('B')
rgb_data = np.zeros((h, w, 3), dtype=np.uint8)

with open("C:\\Users\\Jaco\\Desktop\\MemorySnapshot1", 'rb') as f:
    i = 0;
    raw_data.fromfile(f, byte_count)
    r = 0
    while(r < 320):
        c = 0
        while(c < 240):
            #print((str(r) + "," + str(c)) + ":" + str(i))
            rgb_data[r][c][2] = raw_data[i]
            i += 1
            rgb_data[r][c][1] = raw_data[i]
            i += 1
            rgb_data[r][c][0] = raw_data[i]
            i += 2
            c += 1
        r += 1
        
#print(raw_data)
#print(rgb_data)


#data = np.zeros((h, w, 3), dtype=np.uint8)
#data[0:256, 0:256] = [255, 0, 0] # red patch in upper left

img = Image.fromarray(rgb_data, 'RGB')
img.save('my.png')
img.show()   