#ifndef PRAC1_FFT_AND_DISPLAY_H
#define PRAC1_FFT_AND_DISPLAY_H

//Includes
#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"
#include "math.h"
#include "complex.h"
#include "stdbool.h"
#include "stdio.h"
//#include "string.h"

//Function Prototypes
void fp_dft(double complex*, const double*, const int);
void FirstPrinciples_DFT(double*, const double*, const int);
void GenerateTwiddleFactors(double complex*, const int);
void fp_fft(double complex*, const double*, const int, const int, const double complex*);
void FirstPrinciples_FFT(double*, const double*, const int);
void FirstPrinciples_FFT_LUT(double*, const double*, const int, const double complex*);
void DrawLayout();
void DrawFFT(const double*, const int, const bool);

#endif
