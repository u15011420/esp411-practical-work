/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Prac1_FFT_and_Display.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#include <stdio.h>
#include <math.h>
#include <complex.h>

typedef double complex cplx;

uint16_t MAX_X;
uint16_t MAX_Y;


/*void fp_dft(double complex* signal_dft, const double* input_signal, const int N)
{
	for (int m = 0; m < N; m++)
	{
		for (int n = 0; n < N; n++)
		{
			signal_dft[m] = signal_dft[m] + input_signal[n]*cexp((-I*2*M_PI*m*n)/N);
		}
	}

	return;
}

void FirstPrinciples_DFT(double* signal_dft_magnitudes, const double* input_signal, const int N)
{
	double complex signal_dft[N];
	for (int i = 0; i < N; i++) {signal_dft[i] = 0.0 + 0.0*I;}

	fp_dft(signal_dft, input_signal, N);

	double re = 0.0;
	double im = 0.0;
	for (int i = 0; i < N; i++)
	{
		re = creal(signal_dft[i]);
		im = cimag(signal_dft[i]);
		signal_dft_magnitudes[i] = sqrt(re*re + im*im);
	}

	return;
}

void menerate_w_factors(double complex* w_factors, const int N)
{
	for (int k = 0; k < N/2; k++)
	{
		w_factors[k] = cexp((-I*2*M_PI*k)/N);
	}

	return;
}

void fp_fft(double complex* signal_fft, const double* input_signal, const int N, const int N_max, const double complex* w_factors)
{
	if (N == 1)
	{
		signal_fft[0] = input_signal[0] + 0.0*I;
		return;
	}
	else
	{
		double input_signal_even[N/2];
		double input_signal_odd[N/2];
		double complex signal_even_fft[N/2];
		double complex signal_odd_fft[N/2];
		for (int i = 0; i < N/2; i++)
		{
			signal_even_fft[i] = 0.0 + 0.0*I;
			signal_odd_fft[i] = 0.0 + 0.0*I;
			input_signal_even[i] = input_signal[2*i];
			input_signal_odd[i] = input_signal[(2*i)+1];
		}
		fp_fft(signal_even_fft, input_signal_even, N/2, N_max, w_factors);
		fp_fft(signal_odd_fft, input_signal_odd, N/2, N_max, w_factors);
		for (int k = 0; k < N/2; k++)
		{
			double complex w_factor = w_factors[k*N_max/N];
			signal_fft[k] = signal_even_fft[k] + (w_factor*signal_odd_fft[k]);
			signal_fft[k+(N/2)] = signal_even_fft[k] - (w_factor*signal_odd_fft[k]);
		}
	}

	return;
}

void FirstPrinciples_FFT(double* signal_fft_magnitudes, const double* input_signal, const int N)
{
	double complex signal_fft[N];
	for (int i = 0; i < N; i++) {signal_fft[i] = 0.0 + 0.0*I;}
	double complex w_factors[N/2];
	menerate_w_factors(w_factors, N);

	fp_fft(signal_fft, input_signal, N, N, w_factors);

	double re;
	double im;
	for (int i = 0; i < N; i++)
	{
		re = creal(signal_fft[i]);
		im = cimag(signal_fft[i]);
		signal_fft_magnitudes[i] = sqrt(re*re + im*im);
	}

	return;
}

void FirstPrinciples_FFT_LUT(double* signal_fft_magnitudes, const double* input_signal, const int N, const double complex* w_factors)
{
	double complex signal_fft[N];
	for (int i = 0; i < N; i++) {signal_fft[i] = 0.0 + 0.0*I;}

	fp_fft(signal_fft, input_signal, N, N, w_factors);

	double re;
	double im;
	for (int i = 0; i < N; i++)
	{
		re = creal(signal_fft[i]);
		im = cimag(signal_fft[i]);
		signal_fft_magnitudes[i] = sqrt(re*re + im*im);
	}

	return;
}


void display_fft(double* buffer)
{
	if(MAX_X >= 32  && MAX_X <= 236 && MAX_Y >= 32 && MAX_Y <= 316)
	{
		BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
		BSP_LCD_DrawCircle(MAX_X, MAX_Y, 4);
	}
	uint16_t line_lengths[FFT_SIZE];
	for(uint16_t i = 0; i < FFT_SIZE/2; i++)
	{
		line_lengths[i] = (uint16_t)buffer[i];
		if(line_lengths[i] > 200)
			line_lengths[i] = 200;
		if(line_lengths[i] == 0)
			line_lengths[i] = 1;
	}
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTCYAN);
	for(uint16_t i = 0; i < FFT_SIZE/2; i++)
		BSP_LCD_DrawLine(32, 32 + i, 32 + line_lengths[i], 32 + i);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	for(uint16_t i = 0; i < FFT_SIZE/2; i++)
		BSP_LCD_DrawLine(32 + line_lengths[i], 32 + i, 208, 32 + i);
	for(uint16_t i = 0; i < FFT_SIZE/2; i++)
	{
		if(line_lengths[i] > MAX_Y)
		{
			MAX_Y = 32 + i;
			MAX_X = 32 + line_lengths[i];
		}
	}
	//BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
	//BSP_LCD_DrawCircle(MAX_X, MAX_Y, 4);
	return;
}*/

void service_fft()
{

	if(FFT_DISPLAY_READY)
	{
	  //HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
	  //cplx buffer[FFT_SIZE];
	  //From the mbed user manual:
		//The input data is complex and contains 2*fftLen interleaved values as shown below. {real[0], imag[0], real[1], imag[1],..}
	  //for(uint8_t i = 0; i < FFT_SIZE; i++)
	  //{
		  //samples[2*i + 1] = 0.0;
		  //samples[2*i] = (float32_t)*(Sample_Buffer + i)/4096.0; //Every second value is the complex part, set it to zero.
		//  buffer[i] = (float32_t)*(Sample_Buffer + i)*3.3/4096.0;
	  //}

	  //fft(buffer, FFT_SIZE);
	  //arm_cfft_radix2_f32(&cfft_radix2_inst_f32, samples);
	  //arm_cfft_radix2_q31(&cfft_radix2_inst_q31, samples);

	  double input_buffer[FFT_SIZE];
	  double output_buffer[FFT_SIZE];
	  for(uint16_t i = 0; i < FFT_SIZE; i++)
		  input_buffer[i] = ((double)*(Sample_Buffer_Global + i)*3.3*0)/(4096.0*FFT_SIZE);

	  FirstPrinciples_FFT_LUT(output_buffer, input_buffer, FFT_SIZE, Twiddle_Factors);
	  //HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);
	  //display_fft(output_buffer);
	  DrawFFT(output_buffer, FFT_SIZE, false);
	}
}



/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_dac2;
extern DMA2D_HandleTypeDef hdma2d;
extern LTDC_HandleTypeDef hltdc;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim14;

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 stream6 global interrupt.
  */
void DMA1_Stream6_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream6_IRQn 0 */

  /* USER CODE END DMA1_Stream6_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_dac2);
  /* USER CODE BEGIN DMA1_Stream6_IRQn 1 */

  /* USER CODE END DMA1_Stream6_IRQn 1 */
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */
  /* USER CODE END TIM2_IRQn 0 */
  HAL_TIM_IRQHandler(&htim2);
  /* USER CODE BEGIN TIM2_IRQn 1 */

  /* USER CODE END TIM2_IRQn 1 */
}

/**
  * @brief This function handles TIM8 trigger and commutation interrupts and TIM14 global interrupt.
  */
void TIM8_TRG_COM_TIM14_IRQHandler(void)
{
  /* USER CODE BEGIN TIM8_TRG_COM_TIM14_IRQn 0 */

  /* USER CODE END TIM8_TRG_COM_TIM14_IRQn 0 */
  HAL_TIM_IRQHandler(&htim14);
  /* USER CODE BEGIN TIM8_TRG_COM_TIM14_IRQn 1 */

  /* USER CODE END TIM8_TRG_COM_TIM14_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream4 global interrupt.
  */
void DMA2_Stream4_IRQHandler(void)
{
	/* USER CODE BEGIN DMA2_Stream4_IRQn 0 */
	HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_SET);
	for(uint16_t i = 0; i < FFT_SIZE; i++)
	  Sample_Buffer_Local[i] = Sample_Buffer_Global[i];
	HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_RESET);

	//Prepare input for FFT
	for(uint16_t i = 0; i < FFT_SIZE; i++)
		input_buffer[i] = ((double)*(Sample_Buffer_Local + i));
	//HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
	//FirstPrinciples_FFT_LUT(output_buffer, input_buffer, FFT_SIZE, Twiddle_Factors);
	//HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);

	//FFT calculation
	HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
	for(uint16_t i = 0; i < FFT_SIZE; i++)
	{
		fft_buffer[2*i + 1] = 0.0;
		fft_buffer[2*i] = (float32_t)input_buffer[i]/4095.0;
	}
	cfft_radix2_inst_f32.ifftFlag = 0;
	//cfft_radix2_inst_f32.bitReverseFlag = 0;
	arm_cfft_radix2_f32(&cfft_radix2_inst_f32, fft_buffer);
	arm_cmplx_mag_f32(fft_buffer, fft_magnitude_buffer, FFT_SIZE);
	HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);

	//Filtering

	//DAC Buffering
	for(uint16_t i = 0; i < FFT_SIZE; i++)
		dac_buffer_secondary[i] = (uint16_t)input_buffer[i];

	//DAC Output
	for(uint16_t i = 0; i < FFT_SIZE; i++)
		dac_buffer_primary[i] = dac_buffer_secondary[i];

	/* USER CODE END DMA2_Stream4_IRQn 0 */
	HAL_DMA_IRQHandler(&hdma_adc1);
	/* USER CODE BEGIN DMA2_Stream4_IRQn 1 */

	/* USER CODE END DMA2_Stream4_IRQn 1 */
}

/**
  * @brief This function handles LTDC global interrupt.
  */
void LTDC_IRQHandler(void)
{
  /* USER CODE BEGIN LTDC_IRQn 0 */

  /* USER CODE END LTDC_IRQn 0 */
  HAL_LTDC_IRQHandler(&hltdc);
  /* USER CODE BEGIN LTDC_IRQn 1 */

  /* USER CODE END LTDC_IRQn 1 */
}

/**
  * @brief This function handles DMA2D global interrupt.
  */
void DMA2D_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2D_IRQn 0 */

  /* USER CODE END DMA2D_IRQn 0 */
  HAL_DMA2D_IRQHandler(&hdma2d);
  /* USER CODE BEGIN DMA2D_IRQn 1 */

  /* USER CODE END DMA2D_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
